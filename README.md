<h1 align="center">:file_cabinet: Store theme app</h1>

## :memo: Descripción
Store Theme esta siendo desarrollado siguiendo las lecciones del Learning Path de Vtex. Los bloques cursados para realizar la Store Theme son:<b>Basic Block</b>, <b>Complex Layout</b>, <b>Making Your Store Unique</b> y <b>Site Editor and Content</b>

## :books: Store Component Apps
*   - [Header]()
    - [Home]()
    - [Product]()
    - [About-us]()
    - [Footer]()
    - [Search]()
    - [Slider-layout]()
    


## :wrench: Tecnologias utilizadas
*   <b>Vtex CLI</b>
    <b>React js</b>
    <b>SCSS</b>


## :rocket: Levantar el proyecto
Para rodar o repositório é necessário clonar o mesmo, dar o seguinte comando para iniciar o projeto:
```
<git clone https://gitlab.com/Alfredo_Supanta/vtex-course>
<vtex login xxxxxx>
<vtex link>
```

## :handshake: Colaboradores
<table>
  <tr>
    <td align="center">
      <a href="https://github.com/AlfredoMarcelo">
        <img src="https://avatars.githubusercontent.com/u/82328683?s=96&v=4" width="100px;" alt="Foto de Alfredo en GitHub"/><br>
        <sub>
          <b>Alfredo Supanta</b>
        </sub>
      </a>
    </td>
  </tr>
</table>
